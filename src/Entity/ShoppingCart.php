<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShoppingCartRepository")
 */
class ShoppingCart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $shopping_list;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ShoppingList", mappedBy="shoppingCart")
     */
    private $shoppingLists;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ShoppingList", mappedBy="shoppingList", orphanRemoval=true)
     */
    private $shoppingList;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="shoppingcart_product")
     */
    private $product_shoppingcart;

    public function __construct()
    {
        $this->shoppingLists = new ArrayCollection();
        $this->shoppingList = new ArrayCollection();
        $this->product_shoppingcart = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getProduct(): ?string
    {
        return $this->product;
    }

    public function setProduct(string $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getShoppingList(): ?string
    {
        return $this->shopping_list;
    }

    public function setShoppingList(string $shopping_list): self
    {
        $this->shopping_list = $shopping_list;

        return $this;
    }

    /**
     * @return Collection|ShoppingList[]
     */
    public function getShoppingLists(): Collection
    {
        return $this->shoppingLists;
    }

    public function addShoppingList(ShoppingList $shoppingList): self
    {
        if (!$this->shoppingLists->contains($shoppingList)) {
            $this->shoppingLists[] = $shoppingList;
            $shoppingList->setShoppingCart($this);
        }

        return $this;
    }

    public function removeShoppingList(ShoppingList $shoppingList): self
    {
        if ($this->shoppingLists->contains($shoppingList)) {
            $this->shoppingLists->removeElement($shoppingList);
            // set the owning side to null (unless already changed)
            if ($shoppingList->getShoppingCart() === $this) {
                $shoppingList->setShoppingCart(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProductShoppingcart(): Collection
    {
        return $this->product_shoppingcart;
    }

    public function addProductShoppingcart(Product $productShoppingcart): self
    {
        if (!$this->product_shoppingcart->contains($productShoppingcart)) {
            $this->product_shoppingcart[] = $productShoppingcart;
            $productShoppingcart->setShoppingcartProduct($this);
        }

        return $this;
    }

    public function removeProductShoppingcart(Product $productShoppingcart): self
    {
        if ($this->product_shoppingcart->contains($productShoppingcart)) {
            $this->product_shoppingcart->removeElement($productShoppingcart);
            // set the owning side to null (unless already changed)
            if ($productShoppingcart->getShoppingcartProduct() === $this) {
                $productShoppingcart->setShoppingcartProduct(null);
            }
        }

        return $this;
    }
}
